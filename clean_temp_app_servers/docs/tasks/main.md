



# main.yml


* No description available for this task - here is the definition:  
```

debug:
  msg: 'Clean temp folders for server: {{inventory_hostname}}'
  
```

* Run shell command to delete files older than n days in webapi logs folder

* Run shell command to delete files older than n days in webapi archives logs folder

* Run shell command to delete files older than n days in Filestorage Reports folder

* Run shell command to delete logs n days old in the \Temp\LOGS directory

* find all teamsys.ini files

* No description available for this task - here is the definition:  
```

debug:
  msg: 'Customer path: {{ cs_root_path }}{{ item[2] }}'
loop: '{{ my_sequence | sort }}'
when: iniFolder.stat.exists == True and listIni.matched | int > 0
  
```

* Run shell command to delete files older than n days in customer folder