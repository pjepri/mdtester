# Ansible Role: clean_temp_app_servers

Simple play that deletes logs {{n}} days older in the wevapi archives logs folder 

## Actions:

Actions performed by this role


#### Cleanlogs:
* The first play cleans the logs in the webAPI folder 

## Tags:
## Variables:

* `daysToPreserve`: `7` - 


## TODO:

#### Improvement:
* Add play to delete logs older than {{n}} days in C:\Temp\LOGS -  
#### Bug:
* Second play not working (low priority) -  

## Author Information
This role:  was created by: Elia curti

Documentation generated using: [Ansible-autodoc](https://github.com/AndresBott/ansible-autodoc)

